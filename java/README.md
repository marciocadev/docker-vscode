# Instruções

## Desenvolvimento

Ao se executar o comando abaixo a imagem de desenvolvimento será gerada, esta imagem esta sendo gerada em um container com o java JDK e se permite o debug no vscode (ver o diretório .vscode)

Constroe a imagem a partir do serviço java-dev especificado no docker-compose.yml (docker-vscode_java-dev)
```
docker-compose build java-dev
```

Coloca o container java-dev em execução que é gerado a partir da imagem docker-vscode_java-dev
```
docker-compose up java-dev
```

**Acesse a url localhost:4000 para acessar a aplicação, o modo debug pode ser utilizado marcando um breakpoint e dando play na configuração "Java: Remote Attach" no debug do VSCode**

### Adicionais do desenvolvimento

Caso seja necessário verificar internamente o container gerado pode-se usar os comandos abaixo para acessar o mesmo:

Executar os testes no container sem persister o container de teste
```
docker-compose run --rm java-dev ./mvnw test
```

Executa o container em modo iterativo e abre o shell do container (sh), não persiste o container
```
docker-compose run --rm java-dev sh
```

## Produção

Ao se executar o comando abaixo a imagem de produção será gerada, esta imagem esta sendo gerada em um container com o java JRE

Constroe a imagem a partir do serviço java-prod especificado no docker-compose.yml (docker-vscode_java-prod)
```
docker-compose build java-prod
```

Coloca o container java-prod em execução que é gerado a partir da imagem docker-vscode_java-prod
```
docker-compose build java-prod
```

**Acesse a url localhost:4000 para acessar a aplicação**

## Comandos adicionais

Listar imagens
- docker image ls
Ao se executar o comando acima após a construção das duas imagens vc encontrará 4 imagens conforme abaixo:
- openjdk (imagem java jdk versão 8)
  - TAG: 8-jdk-alpine
  - SIZE: 105 MB
- openjdk (imagem java jre versão 8)
  - TAG: 8-jre-alpine
  - SIZE: 84.9 MB
- java-dev-img (imagem gerada a partir do java jdk, foi baixada as dependências via maven e gerado o build da aplicação)
  - TAG: 1.0.0
  - SIZE: 259 MB
- java-prod-img (imagem gerada a partir do java jre, foi copiado o jar criado no ambiente de desenvolvimento com as dependências)
  - TAG: 1.0.0
  - SIZE: 104 MB

