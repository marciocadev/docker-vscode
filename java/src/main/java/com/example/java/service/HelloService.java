package com.example.java.service;

import org.springframework.stereotype.Service;

@Service
public class HelloService {
    public String greet() {
        return "Hello Docker World";
    }
}