package com.example.java;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.example.java.controller.HelloController;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SmokeTest {

    @Autowired
    private HelloController controller;

    @Test
    public void contextLoads() throws Exception {
        assertNotNull(controller);
    }
}